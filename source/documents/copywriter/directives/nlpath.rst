******
nlpath
******

Description
===========

nlpath directive will generate a link (a href element) to an internal page. 

This directive is ONLY useful for documents that will be parsed and dumped to Nilead's blog

The main difference of this directive compares to nldocpath is that it allows passing a custom route instead of using the default blog route.

For other type of linkings, please refer to Sphinx's default syntax

Syntax
======

.. code-block:: rst

    @param string :nlpath: must present in the string
    @param string (class=abc,style=[xyz:123;xyza:1234]) is the optional attributes of the target html element
    @param string route_name is the route to the target document. This must be a valid Nilead route
    @param string (a:b,x:y) is the optional route paramters. 
    @param string  /title here/ is the  title of the link. If the title contains white space it should be put in betweens the forward slashes as shown in the example. Otherwise the forward slashes can be ignored
    
    @example
    // full example
    // return <a class="abc" style="xyz:'123'; xyza:'1234'" href="{{ path('route_name', {a: 'b', x: 'y'}) }}">title here</a>
    :nlpath(class=abc,style=[xyz:123;xyza:1234]):route_name(a:b,x:y):/title here/
    
    @example
    // no attributes
    // return <a href="{{ path('route_name', {a: 'b', x: 'y'}) }}">title here</a>
    :nlpath:route_name(a:b,x:y):/title here/

    @example
    // no attributes and no paramters
    // return <a href="{{ path('route_name', {}) }}">title here</a>
    :nlpath:route_name:/title here/

    @example
    // no attributes and single word title
    // return <a href="{{ path('route_name', {}) }}">title</a>
    :nlpath:route_name:title

