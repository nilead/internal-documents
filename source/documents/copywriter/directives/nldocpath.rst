*********
nldocpath
*********

Description
===========

nldocpath directive will generate a link (a href element) to an internal document. 

This directive is ONLY useful for documents that will be parsed and dumped to Nilead's blog

For other type of linkings, please refer to Sphinx's default syntax

Syntax
======

.. code-block:: rst

    @param string :nldocpath: must present in the string
    @param string (class=abc,style=[xyz:123;xyza:1234]) is the optional attributes of the target html element
    @param string relative/document/path is the relative path to the target document. This path must be relative to the root documents/ folder
    @param string  /title here/ is the  title of the link. If the title contains white space it should be put in betweens the forward slashes as shown in the example. Otherwise the forward slashes can be ignored

    @example
    // full example
    // return <a class="abc" style="xyz:'123'; xyza:'1234'" href="{{ path('nilead.blog.frontend.blog.show', {slug: 'relative-document-path'}) }}">title here</a>
    :nldocpath(class=abc,style=[xyz:123;xyza:1234]):relative/document/path:/title here/

    @example
    // no attributes
    // return <a href="{{ path('nilead.blog.frontend.blog.show', {slug: 'relative-document-path'}) }}">title here</a>
    :nldocpath:relative/document/path:/title here/

    @example
    // no attributes and single word title
    // return <a href="{{ path('nilead.blog.frontend.blog.show', {slug: 'relative-document-path'}) }}">title</a>
    :nldocpath:relative/document/path:title