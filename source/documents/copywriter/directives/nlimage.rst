*******
nlimage
*******

Description
===========

nlimage directive will generate an image (an img element) to an image. 

This directive is ONLY useful for documents that will be parsed and dumped to Nilead's blog.

For other type of images, please refer to Sphinx's default syntax

Syntax
======

.. code-block:: rst

    @param string :nlimage: must present in the string
    @param string (class=abc,style=[xyz:123;xyza:1234]) is the optional attributes of the target html element
    @param string relative/path/to_image is the relative path to the target image. This path should be relative to _static/images folder
    @param string  /title here/ is the alt text of the image. If the title contains white space it should be put in betweens the forward slashes as shown in the example. Otherwise the forward slashes can be ignored
    
    @example
    // full example
    // return <img class="abc" style="xyz:'123'; xyza:'1234'" src="path/to_image" alt="title here"/>
    :nlimage(class=abc,style=[xyz:123;xyza:1234]):relative/path/to_image:/title here/
    
    @example
    // no attributes
    // <img src="path/to_image" alt="title here"/>
    :nlimage:relative/path/to_image:/title here/

    @example
    // no attributes and single word title
    // <img src="path/to_image" alt="title"/>
    :nlimage:relative/path/to_image:title

    @example
    // no attributes and no title. Image path will be used for alt text
    // <img src="path/to_image" alt="relative/path/to_image"/>
    :nlimage:relative/path/to_image: