********************
Search sample config
********************

.. code-block:: yaml
	    
	nilead_search:
	    settings:
	        global:
	            engine: elastic
	            min_score: 0
	            targets: ['product', 'party', 'order']
	            preset:
	                default:
	                    queries:
	                        global:
	                            type: multi_match
	                            field: ['name', 'name.*', 'purchaser_email^2']
	                            options:
	                                type: most_fields

	        product:
	            engine: elastic # the search engine to use, only elastic is supported now
	            # Here we define the list of fields and the possible queries, filters, aggs they allow
	            min_score: 0
	            targets: ['product']
	            # Preset allow us to setup the list of all possible search scenarios so that
	            # we can build appropriate forms for search/filter
	            preset:
	                default:
	                    queries:
	                        name_match:
	                            type: multi_field
	                            fields:
	                                - field: name.ngram
	                                  type: match
	                                - field: name
	                                  type: match
	                    filters:
	                        category_terms:
	                            field: category
	                            type: terms
	                            agg:
	                                type: path_terms
	                                transformer: category
	                        type_term:
	                            field: type
	                            type: term
	                            agg:
	                                type: terms
	                        enabled_term:
	                            field: enabled
	                            type: term
	                            agg:
	                               type: terms
	                        option_value_terms:
	                            field: option_value
	                            type: terms
	                            agg:
	                                type: terms
	                                transformer: option_value
	                        updated_number_range:
	                            field: updated
	                            type: number_range
	                            agg:
	                                type: stats
	                        price_number_range:
	                            field: price
	                            type: number_range
	                            agg:
	                                type: stats
	                simple:
	                    queries:
	                        name_match:
	                            type: multi_match
	                            field: ['name', 'name.*']
	                            options:
	                                type: best_fields

	        order:
	            engine: elastic
	            targets: ['order']
	            preset:
	                default:
	                    queries:
	                        purchaser_email_term:
	                            field: purchaser_email
	                            type: term
	                            agg:
	                                type: terms
	                        purchfaser_name_term:
	                            field: purchaser_name
	                            type: term
	                            agg:
	                                type: terms
	                        type_term:
	                            field: type
	                            type: term

	# mapping to form type
	    form_mapping:
	        queries:
	            match: nilead_search_query_match
	            multi_match: nilead_search_query_match
	            wildcard: nilead_search_query_wildcard
	        filters:
	            term: nilead_search_filter_term
	            terms: nilead_search_filter_term
	            number_range: nilead_search_filter_number_range
	            range: nilead_search_filter_range
	        aggs:
	            default: nilead_search_agg
	        sorters:
	            default: nilead_search_sorter
