**************************
FOS Elastica sample config
**************************

To understand ElasticSearch's related configs you **MUST** read `ElasticSearch documents`_ 

.. _ElasticSearch documents: http://www.elasticsearch.org/resources/

.. code-block:: yaml

    fos_elastica:
	    clients:
	        default: { host: localhost, port: 9200 }
	    indexes:
	        nilead:
	            finder: ~
	            settings:
	                analysis:
	                    analyzer: # configure or add more analyzer if you need. This is very important to get the desired results
	                        startswith:
	                            tokenizer: keyword
	                            filter: ["lowercase", "asciifolding"]
	                        phonetic_analyzer: # useful for analyzing names. Note that you must install an elastic plugin for this to work
	                            type: "custom"
	                            tokenizer: "lowercase"
	                            filter: ["name_metaphone", "standard", "asciifolding"]
	                        ngram_analyzer: # useful to allow searching parts of words
	                            type: "custom"
	                            tokenizer: "lowercase"
	                            filter   : ["name_ngram", "asciifolding"]
	                    filter:
	                        name_metaphone:
	                            encoder: "metaphone"
	                            replace: false
	                            type: "phonetic"
	                        name_ngram:
	                            type: "nGram"
	                            min_gram: 2
	                            max_gram: 4
	            client: default
	            index_name: nilead
	            types:
	                product:
	                    mappings:
	                        id: ~
	                        name:
	                            type: multi_field # it's possible that you want to index one field multiple ways to service searching purpose
	                            fields:
	                                ngram:
	                                    type: string
	                                    analyzer: ngram_analyzer
	                        description:
	                            type: string
	                            search_analyzer: startswith
	                            index_analyzer: startswith
	                        price:
	                            type: integer
	                        createdAt:
	                            type: date
	                            format: basic_date_time
	                    persistence:
	                        elastica_to_model_transformer:
	                            ignore_missing: true # ignore missing is useful in case the results returned fron elastic somehow contain entities that do not exist in database (delayed sync for example)
	                            service: nilead.product.search.engine.elastic.product_transformer # here you can point to your customized transformer for each document type. The transformer is responsible to transform elastic results to whatever form you desire.
	                        driver: orm
	                        model: %nilead.product.model.product.class%
	                        finder: ~ # finder should be set like this so that a finder service is created for us. This is not necessary if you use your own finder (as we plan to do so in the near future)
	                party:
	                    mappings:
	                        id: ~
	                        name:
	                            type: multi_field
	                            fields:
	                                ngram:
	                                    type: string
	                                    analyzer: ngram_analyzer
	                                phonetic:
	                                    type: string
	                                    analyzer: phonetic_analyzer
	                        legal_name:
	                            type: string
	                            search_analyzer: startswith
	                            index_analyzer: startswith
	                        tax_id:
	                            type: string
	                    persistence:
	                        elastica_to_model_transformer:
	                            ignore_missing: true
	                            service: nilead.party.search.engine.elastic.party_transformer
	                        driver: orm
	                        model: %nilead.party.model.party.class%
	                        finder: ~