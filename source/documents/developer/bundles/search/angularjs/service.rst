**************
Search Service
**************

The search service is structured similarly to the bulk service. The SearchFactory is located in nilead/search common lib, if you want to customize the behavior of SearchService you are recommended to read the code in our repo. This Factory should not be used directly but should be used in a service to generate a search instance.

An example of a search service that use the above Factory:

.. code-block:: javascript

    service('ProductSearch', ['Product', 'SearchFactory', function (Product, SearchFactory) {
        var $searches = {};

        // Here we create a multiton that allows us to re-use a search instance 
        // The preset here corresponds with the preset defined in our yml configuration for the bundle
        return function (id, preset) {

            if('' == id || !$searches.hasOwnProperty(id)) {

                if (typeof preset === 'undefined') {
                    preset = 'default';
                }

                // the most important piece of the service is here where we return a new instance from SearchFactory
                $searches[id] = new SearchFactory(Product, {
                    presetPath: ['product', preset]
                });
            }

            return $searches[id];
        }
    }])

To start using our new service, we should start first with our app code:

.. code-block:: javascript

    state('product_index', {
            url: '/product/:page',
            params: {
                page: {
                    value: 1
                },
                limit: {
                    value: 3
                }
            },
            controller: 'ProductIndexCtrl',
            templateProvider: function (Resource, $stateParams, $state, ProductSearch) {

                var url = Routing.generate('nilead.product.backend.product.index',
                    angular.extend({
                        _format: 'partial',
                        include_metadata: true,
                        paginate: true
                    }, $stateParams, ProductSearch('listing').getParams()));

                return Resource.get(url).then(function (response) {
                    if (response.data.hasOwnProperty('data')) {
                        ProductSearch('listing').processData(response.data.data);
                    }

                    return response.data.template;
                });
            }
        })