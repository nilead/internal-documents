**********************
SearchSimple directive
**********************

Located in ``nilead.module.search``


Purpose
=======

Support the simple search single text field search.


Usage
======

Apps that use this directive should include ``nilead.module.search``

.. code-block:: html

    Requirements: [nlSearcher, nlModel, nlInput, nlTarget]

+------------+-------------+-------------------------------------------------------------------+
| Variable   | Requirement | Description                                                       |
+============+=============+===================================================================+
| nlSearcher | required    | A SearchFactory's generated                                       |
+------------+-------------+-------------------------------------------------------------------+
| nlModel    | required    | A string that refers to the search query/filter key               |
+------------+-------------+-------------------------------------------------------------------+
| nlInput    | optional    | A jQuery style selector string that refers to the input field. If |
|            |             | none is passed, this directive will assume current element to be  |
|            |             | the input field                                                   |
+------------+-------------+-------------------------------------------------------------------+
| nlTarget   | required    | A jQuery style selector string that refers to the dom where the   |
|            |             | search result should put into                                     |
+------------+-------------+-------------------------------------------------------------------+

**Example**

.. code-block:: html

    <div data-nl-search-simple data-nl-model="queries.name_match.value" data-nl-input="#checkout_product_search" data-nl-target="#checkout_product_search_result" data-nl-searcher="checkoutProductSearcher" >
        <input id="checkout_product_search" type="text">
                
        <div id="checkout_product_search_result"></div>
    </div>		