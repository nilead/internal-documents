*****************
Select2 directive
*****************

Located in ``nilead.module.search``


Purpose
=======

Support the select2 directive to use search for getting data.


Usage
======

Apps that use this directive should include ``nilead.module.search``

.. code-block:: html

    Requirements: [nlSearchSelect2, nlSearcher]

+----------------------+-------------+---------------------------------------------------+
| Variable             | Requirement | Description                                       |
+======================+=============+===================================================+
| nlSearchSelect2      | required    | The path to set the search data                   |
+----------------------+-------------+---------------------------------------------------+
| nlSearcher           | required    | A SearchFactory's generated                       |
+----------------------+-------------+---------------------------------------------------+
| nlSelectOptions      | optional    | Select2 options                                   |
+----------------------+-------------+---------------------------------------------------+
| nlSearchParams       | optional    | Search parameters                                 |
+----------------------+-------------+---------------------------------------------------+
| nlMapping            | optional    | Mapping search result for id and text values for  |
|                      |             | select2                                           |
+----------------------+-------------+---------------------------------------------------+
| template             | required    | This is where the code for the real select2 input |
|                      |             | should be placed                                  |
+----------------------+-------------+---------------------------------------------------+

**Example**

.. code-block:: html

    <!-- It's important to note how the directive contains the select2 input in its attribute -->
    <div data-nl-search-select2="queries.name_match.value" data-nl-searcher="searcher" data-nl-mapping="{default: {id: party_id, text: name}}" data-form="form" data-ng-attr-template='{{ form_widget(prototype.products ,{"attr": { "data-ui-select2":"selectOptions({ multiple: true })" } }) }}'></div>