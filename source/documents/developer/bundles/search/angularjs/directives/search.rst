****************
Search directive
****************

Located in ``nilead.module.search``


Purpose
=======

Support complex search and filter form


Usage
======

Apps that use this directive should include ``nilead.module.search``

There are 2 directives to use: ``nlSearch`` and ``nlSearchInput``

nlSearch
----------

This directive is used to display the whole filter form (which is rendered and passed from the server)

.. code-block:: html

    Requirements: [nlSearcher, nlCallback]

+------------+-------------+---------------------------------------------------------+
| Variable   | Requirement | Description                                             |
+============+=============+=========================================================+
| nlSearcher | required    | A SearchFactory's generated                             |
+------------+-------------+---------------------------------------------------------+
| nlCallback | optional    | A callback function that will be called when search is  |
|            |             | triggered. This function will receive the searcher as   |
|            |             | its parameter                                           |
+------------+-------------+---------------------------------------------------------+

**Example**

.. code-block:: html

    <div data-nl-seach data-nl-searcher="searcher"></div>

nlSearchInput
---------------

This directive is used to toggle the individual filter input status (enabled/disabled) based on the available filters passed back from the server. This directive also disables the inputs upon search-start and re-enable the inputs (if applicable) upon search-end.

The following 3 parameters should be passed via ng-attr

.. code-block:: html

	Requirements: [aggKey, value, type]

+------------+-------------+---------------------------------------------------------+
| Variable   | Requirement | Description                                             |
+============+=============+=========================================================+
| aggKey     | required    | The path to the filter/query key                        |
+------------+-------------+---------------------------------------------------------+
| type       | required    | The input type (checkbox, radio, etc..)                 |
+------------+-------------+---------------------------------------------------------+
| value      | required    | The input value                                         |
+------------+-------------+---------------------------------------------------------+

**Example**

.. code-block:: html

    <input nl-searcher-input ng-attr-agg-key="category_path_terms" type="checkbox" data-checklist-model="nlSearcher.$data.filters.category_path_terms.value" data-checklist-value="199" value="199">