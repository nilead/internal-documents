*******
Search
*******

Nilead search feature is provided by the SearchBundle which supports several search engines. As of now, there are 2 search engines supported:

1. `Doctrine`_ 
2. `ElasticSearch`_ 


Understanding the flow
======================

(TODO)

Installation
============

Before using nilead_search, please make sure to setup the FOSElasticaBundle first. You can refer to the following sample configuration :doc:`config/fos_config`

Configuration
=============

nilead_search also requires you to set certain configurations for it to work. You can refer to the following sample configuration :doc:`config/search_config`

Usage
=====

Basic
-----

If you use only the default features and presets of this bundle you don't have to worry about setting any configuration at all since everything has been configured for you. 

Assuming that you are using angularjs, first you need to configure your app to use SearchService to retrieve data similar to this:

.. code-block:: javascript

    .state('product_index', {
            url: '/product/:page',
            params: {
                page: {
                    value: 1
                },
                limit: {
                    value: 3
                }
            },
            controller: 'ProductIndexCtrl',
            templateProvider: function (Resource, $stateParams, $state, ProductSearch) {

                # notice how we are calling ProductSearch('listing') here
                # the search service will use multiton pattern to retrieve the search instance for us
                
                # here we are passing in the pagination and meta query data as SearchService will 
                # not automatically catch those for us
                return ProductSearch('listing').search(angular.extend({
                    _format: 'partial',
                    include_metadata: true,
                    paginate: true
                }, $stateParams)).then(function (response) {
                    if (response.data.hasOwnProperty('data')) {
                        # here we call ProductSearch('listing') again which should return the exact
                        # same instance as above
                        ProductSearch('listing').processData(response.data.data);
                    }

                    return response.data.template;
                });
            }
        })

Assuming you want to use the :doc:`directive` to render your filter in the template, the directive expects the search instance to be passed to it so you can pass it to the scope like this in your corresponding controller:

.. code-block:: javascript

    .controller('ProductIndexCtrl', ['$scope', '$rootScope', '$state', 'Product', 'ProductSearch', function ($scope, $rootScope, $state, Product, ProductSearch) {
        $scope.Product = Product;

        $rootScope.$params = $state.params;

        $scope.searcher = ProductSearch('listing');
    }])

Now we can use our directive in the template:

.. code-block:: html

    <div data-nl-search data-nl-searcher="searcher"></div>


If you want to be able to update the url to reflect new changes when user filter, you can override the search function in your controller (for example) then pass to your directive:

.. code-block:: javascript

    .controller('ProductIndexCtrl', ['$scope', '$state', 'ProductSearch', function ($scope, $state, ProductSearch) {
    
        $scope.searcher = ProductSearch('listing');

        $scope.search = function () {
            // do something here
        };
    }])

.. code-block:: html

    <div data-nl-search data-nl-searcher="searcher" data-nl-search="search()"></div>

.. note:: Note that the search services such as ProductSearch service above accept 2 parameters. The first option is the id (any unique string you want to use) and the second optional parameter is the **preset**. If none is passed, ``default`` will be used.



Custom parser
=============

(TODO)


.. _Doctrine: http://doctrine-project.org
.. _ElasticSearch: http://elasticsearch.org