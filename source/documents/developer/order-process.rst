The order process in Nilead is quite complex under the hood since we support multiple shipments, multiple invoices and multiple transactions.

1. The client adds the purchasable items to cart

2. The client proceeds to checkout

3. A shipment is created for the client
a. The client can choose to break the shipment into multiple shipments (different shipto addresses)
b. The client can choose one shipping method for each shipment

4. Order adjustments (taxes, discounts and such are calculated and applied to the order)

5. An invoice is created for each shipment
a. The client can choose to break the invoice into multiple invoices (this option is not available now)
b. The client can choose one or multiple payment methods for the invoices

6. Transaction is used to record the amount received from, sent to the client. 
a. Transactions are associated directly with order, not with invoice(s)
a. Each transaction is associated with a payment method (such as cash, COD, credit card etc, ...)
b. Each transaction can contribute toward one or multiple invoices and these contributions are recorded as allocations. Each allocation records the amount one transaction contributes toward one invoice.

7. In case of Refund, the specific invoice should be voided. The system should then generate a new invoice if neccessary (partial refund) and should generate a credit memo (which is somewhat like a negative invoice).
a. Similar to Invoice, CreditMemo should originate from a shipment (in this case, a shipment to receive item back to stock)
b. Even in the case 
n

**** There are 3 situations that should happen when invoice(s) is created and payment(s) is made:
1. A transaction is made for 1 single invoice for the entire order (for exmple, in the case of online credit card payment)

2. One (or more) transaction is made for each invoice per shipment in the order (for example, COD payment method)

3. Several (X) transactions are made for several (Y) invoices for multiple shipments 

**** If any order adjustment occurs after payment has already been authorized (for example, if shipping charge occurs), then the store should either try to 
a. Request authorization for surcharge by sending customers a new payment link (this feature is not available yet)
b. Attempt to capture more than authorized (some gateways such as Paypal allows this)