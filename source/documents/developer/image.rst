******
Image
******

On Nilead, to optimize performance all uploaded images are pre-processed and made available in several sizes:

1. thumb (50x50)
2. small (120x120)
3. medium (240x240)
4. large (480x480)
5. grande (640x640)
6. original (the original image but not greater than 2048x2048)