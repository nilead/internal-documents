Introduction to SEO -- 2016-03-02 
=================================

Các khái niệm cơ bản trong SEO
------------------------------

**Algorithm**

Thuật ngữ Algorithm trong các khái niệm SEO cơ bản còn được gọi là thuật toán. Nó được xây dựng để tính toán, giải quyết các dữ liệu và các công việc khác theo trình tự đã được lập trình sẵn dựa trên các thông số nhất định. Đối với SEO, thuật toán này chính là công thức được các công cụ tìm kiếm sử dụng để thu thập, đánh giá và xếp hạng các website theo từ khóa trên các trang hiển thị kết quả tìm kiếm. Trên thực tế, các công cụ tìm kiếm như Google, Yahoo, Bing… đều có 1 thuật toán algorithm khác nhau.

**Domain  Authority (DA)**

Tương tự như Authority Page, Authority Domain cũng là một chỉ số do MOZ (MOZ là 1 công ty chuyên phát triển các phần mềm Marketing) đặt ra. Nếu như Authority Page chỉ tính cho một website cụ thể (webpage) thì Authority Domain được tính cho toàn bộ website, tên miền với độ tin cậy cao dựa trên số lượng liên kết tới trang web như root domain, tổng số backlink, Moztrust, Mozrank cùng nhiều yếu tố khác.

**Page  Authority (PA)**

Authority Page là một chỉ số do MOZ đưa ra để đánh giá chất lượng của 1 trang web có độ uy tín (trust) cao. Việc đánh giá chất lượng này dựa trên rất nhiều tiêu chí xếp hạng của các công cụ tìm kiếm như đánh giá các trang web có cùng chủ đề với nhau hoặc dựa nhiều vào Inbound Link để đánh giá. Chỉ số Authority Page càng cao thì thứ hạng của website đó trên Google càng cao.

**Index (Chỉ mục)**

Index là một quá trình mà các công cụ tìm kiếm tìm được nội dung của bạn để chúng lưu giữ, lập chỉ mục rồi xếp hạng và hiển thị chúng lên SERP. Để biết được website của mình được Google Index hay chưa bạn vào Google Search gõ cú pháp "site:www.domain.com" (thay www.domain.com bằng tên miền của bạn). Ví dụ: site:nilead.com

**Landing page**

Landing page là trang tập trung giới thiệu về một sản phẩm hay dịch vụ , chủ đề nào đó. Nó có chức năng giúp tăng tỷ lệ khách hàng vào trang web và dễ có thứ hạng cao khi được tìm kiếm.

**Từ khóa (Keyword)**

Từ khóa (Keyword) là những từ hay cụm từ có nghĩa mà người dùng sử dụng dùng để truy vấn kết quả trong các bộ máy tìm kiếm.

VÍ dụ: khách hàng có nhu cầu mua điện thoại IPhone 6 thì các từ khóa mà họ dùng nhập vào ô tìm kiếm có thể là các từ như: _iphone 6, iphone 6 chinh hang, iphone 6 xach tay_….

**Penguin**

Penguin là thuật ngữ chim cánh cụt được Google công bố vào ngày 24 tháng 4 năm 2012. THuật toán này ra đời nhằm mục đích trừng phạt các website sử dụng SEO mũ đen như nhồi nhét từ khóa, che đậy, tham gia mua bán trao đổi liên kết, sử dụng nội dụng trùng lặp,…và hướng đến việc đẩy các website chất lượng cao lên trên trong kết quả tìm kiếm.

**Panda**

Panda là một thuật toán Google ra đời nhằm giảm thứ hạng của những trang web có nội dung kém, copy, độ tin cậy kém, đặc biệt là những website vi phạm bản quyền. Mục đích Google cho ra đời thuật toán Panda nhằm giúp cho người làm SEO nỗ lực Onpage, đa dạng hóa nội dung nhằm thu hút người đọc và đẩy website lên trên.

SEO Onpage
----------

**Content (Nội dung website)**

"Content is king - nội dung là vua" nhằm tôn vinh giá trị của nội dung đối với thứ hạng của 1 website. Nếu nội dung website tốt, nhiều người truy cập, tương tác với nội dung đó thì khả năng website đó sẽ có thứ hạng tốt trên kết quả tìm kiếm.

**Canonical Url**

Canonical URL là URL trong đó các Webmaster muốn Google công nhận đây là một địa chỉ chính thức và duy nhất hoặc muốn khách tham quan nhìn thấy. Đây là một phương pháp hiệu quả để ngăn chặn trùng lặp nội dung trong website.

**Duplicate Content**

Duplicate Content là nội dung bị lặp lại trên nhiều trang web khác nhau. Vì các Search Engine muốn nội dung hiển thị phải đa dạng phong phú với kết quả tìm kiếm để giúp người đọc có nhiều sự lựa chọn khác nhau nên google chỉ hiển thị một nội dung duy nhất, tránh sự copy bài của người khác.

**Internal Link**

Internal Link (Liên kết nội bộ) là một trong những yếu tố cực kỳ quan trọng trong quá trình làm SEO, thường được sử dụng để điều hướng trang web. Theo cách hiểu đơn giản thì Internal Link là liên kết từ trang này đến trang khác nằm trong cùng một website.

**Keyword Density**

Keyword Density hay còn gọi là mật độ từ khóa, là tỷ lệ từ khóa hoặc một cụm từ khóa được lặp lại  trên tổng số ký tự của một trang. Nhờ đó mà giúp các  SEOer đánh giá được trang đó mạnh từ khóa nào.

**Meta Title**

Meta Title (tiêu đề trang) hay là thẻ tiêu đề của một webpage trong website. Thẻ tiêu đề là một đoạn mô tả ngắn một cách khái quát nhất về nội dung của website. Thể tiêu đề được hiển thị ngay phần đầu tiên trong trang tìm kiếm kết quả của Google và là yếu tố quan trọng khi đánh giá thứ hạng.

**Meta Description**

Meta Description (mô tả hay miêu tả) dùng để mô tả khái quát nội dung trang web của bạn một cách ngắn gọn nhất nhằm giúp google hiểu một cách tổng quan nhất về nội dung bài viết. Thẻ mô tả là một yếu tố quan trọng trong SEO Onpage và được hiển thị trên SERP nên cần tối ưu cẩn thận.

**Meta Keywords**

Meta Keywords là một trong các thẻ meta (meta tags) của mã HTML trong phần head của một tập tin HTML. Hay nói cách khác, Meta Keyword là một thể dùng để khai báo từ khóa dùng cho bộ tìm kiếm, thông báo cho công cụ tìm kiếm biết website của bạn đang hướng đến nội dung, từ khóa nào.

**Alt**

Thuật ngữ Alt là viết tắt của cụm từ Alternative Text. Đây là một thuộc tính quan trọng của thẻ IMG. Alt là một từ hoặc cụm từ dùng để mô tả hình ảnh trên website hoặc thay thế cho hình ảnh khi không thể được hiển thị. Đối với SEO, thuật ngữ này cũng rất quan trọng. Nó cho phép các công cụ tìm kiếm đọc nội dung để xác định hình ảnh đó nói về điều gì. Trong trường hợp hình ảnh là một liên kết, các SE sẽ xem nội dung của thẻ Alt như là các Anchor Text.

# SEO Offpage

**Redirect 301**

Đây là một phương pháp rất thông dụng dùng để chuyển hướng thông báo tới các trình duyệt và công cụ tìm kiếm đó là trang webpage và website đó đã được di chuyển đến một địa chỉ mới. Mục đích của việc sử dụng phương pháp này là dùng để xử lý lỗi trùng lặp nội dung, thân thiện hơn với các công cụ tìm kiếm và giúp điều hướng người dùng đến địa chỉ mới.

**404 Error**

404 Error là thông báo lỗi rất phổ biến khi một liên kết không tồn tại trong nội dung trên website nhằm báo cho người dùng khi một địa chỉ website không được tìm thấy.

**Htaccess file**

Tập tin .htaccess ( hypertext access ) đây là một file có ở thư mục gốc của các hostting và do apache quản lý, cấp quyền. File .htaccess có thể điều khiển, cấu hình được nhiều thứ với đa dạng các thông số, nó có thể thay đổi được các giá trị được set mặc định của apache. Để xử lý được các trường hợp như redirect 301 hay 404 error ở trên thì file .htaccess này không thể không nhắc tới.

**Robots.txt**

Tương tự như .htaccess file thì Robots.txt là một file nằm trong thư mục gốc của website. Nó chứa toàn bộ nội dung văn bản text có tác dụng thông báo cho các con bọ tìm kiếm biết được nó được phép hay không, dò quét ở đâu và không được ở đâu. Nó không ảnh hưởng đến khả năng dò quét của Spider nên trước khi up lên bạn hãy kiểm tra kỹ để tránh sự cố xảy ra.

**Xml Sitemap**

Xml Sitemap (Sơ đồ website) là một tập tin văn bản có chứa danh sách tất cả các URL của một trang web nhằm thông báo cho các con bọ tìm kiếm biết. Bạn cũng có thể dễ dàng điều chỉnh mức độ quan trọng cho từng trang để thông báo cho bộ máy tìm kiếm biết được đâu là những trang quan trọng trong website. XML có ý nghĩa lớn bởi vì nó giúp cho các con bọ tiết kiệm thời gian tìm kiếm khi thu thập thông tin trên website của bạn. Để biết được trang web của bạn đã có sơ đồ website chưa, bạn làm như sau: Truy cập vào Google Webmaster Tool -> Thu thập dữ liệu -> Sơ đồ trang web.

**Backlink**

Đây là yếu tố quan trọng để đánh giá 1 website. Backlink là những liên kết từ các trang web khác đến trang web của bạn, hay còn gọi là Inbound Link.  Backlink chất lượng sẽ tùy thuộc vào chỉ số DA, PA của trang web khác về vị trí trang web của mình.

**Dofollow**

Dofollow là một dạng liên kết được tính backlinks đến website. Nó là yếu tố để tăng PageRank cho website. Thuộc tính dofollow là liên kết ngược lại với nofollow. Dofollow có dạng rel="dofollow". Khi sử dụng liên kết này trong thẻ a có nghĩa là đã khai báo cho Bot Google và các công cụ tìm kiếm khác biết rằng: liên kết này uy tín truyền Page Rank, hướng Bot đi theo. Một website nếu có nhiều Backlink Dofollow sẽ rất tốt để SEO vì nó tạo được nhiều liên kết với các site khác. Tuy nhiên, nếu quá nhiều Dofollow Links sẽ khiến cho Google xem xét lại tính tự nhiên của website đó. Trong số các khái niệm về SEO thì đây cũng là một khái niệm quan trọng yêu cầu người học phải nắm vững được một cách chính xác.

**Nofollow**

Khi tìm hiểu về SEO thì Nofollow được coi là dạng liên kết không được tính backlinks đến website. Nó không làm tăng cũng như không ảnh hưởng đến PageRank của website. Trong liên kết này ta đặt các thuộc tính rel trong đường dẫn của thẻ a với rel=nofollow.

**Disavow link**

Disavow Link là một công cụ mới của Google Webmaster Tool, nó cho phép loại bỏ các liên kết không chất lượng, liên kết xấu tới trang web của bạn nhằm tạo độ an toàn với Google Penguin. Disavow Link còn giúp website lấy lại thứ hạng của mình sau khi đã loại bỏ hết các liên kết vi phạm thuật toán Penguin.

**Traffic - lưu lượng truy cập:**

Đây cũng là một yếu tố quan trọng không kém. Với lượng traffic cao, website của bạn sẽ có PR ( viết tắt của pagerank) tốt hơn so với những website có lượng traffic thấp hơn. Thông thường, traffic cao và PR cao thường đi chung với nhau, bởi vậy muốn website có PR cao bạn phải đẩy mạnh các hoạt động marketing website để giới thiệu nó đến mọi người.