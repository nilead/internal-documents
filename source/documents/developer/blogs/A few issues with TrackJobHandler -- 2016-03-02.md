A few issues with TrackJobHandler -- 2016-03-02
===============================================

Trong quá trình xem lại phần code của QueueBundle, có một vài vấn đề được phát hiện ra trong phần TrackJobHandler.

Vào thời điểm hiện tại, mô hình handler được sử dụng chủ yếu để tương tác với 1 object nhất định và đóng vai trò như một wrapper của Manager (Doctrine).

Có một số quy luật bất thành văn, ví dụ như hàm update của handler sẽ nhận object cụ thể tương ứng với handler đó. Ví dụ ProductHandler thì method update sẽ nhận ProductInterface object.

Hàm updateJob của TrackJobHandler hiện tại nhận những biến như sau


```php
public function updateJob($jobId, $status, $args = [], $flush = false)
```

Việc pass vào 1 id thay vì một object sẽ khiến hàm này hoạt động khác các hàm tương tự ở các handlers khác, dẫn đến gây khó khăn trong lúc sử dụng.

Trong phần handler này, phía dưới code lại có thêm phần updateFiniteState như sau:

```php
if (in_array($status, $trackJob->getChoices('STATUS_'))) {
            $trackJob->setFiniteState($status);
        } else {
            throw new \Exception(
                sprintf(
                    'Doesn\'t exist %s status',
                    $status
                )
            );
        }
```

Phần này có một vấn đề là code chỗ này thì khá ổn, nhưng liệu có cách nào move nguyên phần checking vào một chỗ chung nào đó, vì nếu nói đến vấn đề updateFiniteState thì sẽ có nhiều objects đều cần tính năng này?

Vấn đề thứ 3 phát sinh trong hàm này chính là việc trigger event, hiện tại phần này nhìn như sau:

```php
$this->eventDispatcher->dispatch('pre_update', new ResourceEvent($trackJob));

        $this->trackJobManager->save($trackJob, $flush);

        $this->eventDispatcher->dispatch('post_update', new ResourceEvent($trackJob));
```

Phần code này tương thích với các handlers khác, nhưng nó cũng nói lên 1 vấn đề là hiện các events này được trigger dựa trên việc các developers có nhớ hay không, và loại events nào cũng như tên events cũng chưa có một naming strategy nào. Việc này sẽ dẫn đến nhiều khó khăn trong việc phát triển sau này.


