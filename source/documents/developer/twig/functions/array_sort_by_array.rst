*******************
array_sort_by_array
*******************

Readily available in all twig templates


Purpose
=======

Sort an array by the values in a second array


Usage
======

.. code-block:: jinja

    {% set array = {'A': {'value' => 1}, 'B': {'value' => 1}, 'C': {'value' => 1}} %}
    {% set sorts = ['B', 'C', 'A'] %}
    {% set groups = array_sort_by_array(groups, sorts[type]) %}

    <!-- Result: -->
    {'B': {'value' => 1}, 'C': {'value' => 1}, 'A': {'value' => 1}}