*****************
Long Term Support
*****************

Each part of code within Nilead project that is used by others either within the Nilead project or other 3rd parties should maintain support during certain period which is called LTS or Long Term Support.

.. epigraph::

   At the beginning of a long-term support period, the software developers impose a feature freeze: They make patches to correct software bugs and vulnerabilities, but do not introduce new features that may cause regression. The software maintainer either distributes patches individually, or packages them in maintenance releases, point releases, or service packs. At the conclusion of the support period, the product reaches end-of-life.

   -- wiki

We define different LTS length for different purposes:

Development branches
====================

+--------------+--------------+-------------+
| Branch       | Type         | Length      |
+==============+==============+=============+
| Development  | Normal       | 2 sprints   |
|              +--------------+-------------+
|              | Important    | 1 sprint    |
|              +--------------+-------------+
|              | Critical     | ASAP        |
+--------------+--------------+-------------+
| Master       | Normal       | 12 sprints  |
|              +--------------+-------------+
|              | Important    | 4 sprint    |
|              +--------------+-------------+
|              | Critical     | ASAP        |
+--------------+--------------+-------------+

.. note:: 1 sprint is estimated to be fixed at 2 business weeks length

