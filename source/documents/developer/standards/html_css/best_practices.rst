*************************
HTML & CSS Best Practices
*************************

These best practices are recommended for all our projects to ensure best quality of work.

Organize your stylesheets
=========================

It's ``VERY`` important to organize your stylesheets for easy reading and maintaining. Please:

1. Group your rules close together
2. Break large set of rules into smaller files (for example, all header's rules can be put in header.css)

Rules can be grouped using the following structure as an example:

1. Resets and overrides
2. Links and type
3. Main layout
4. Secondary layout structures
5. Form elements
6. Miscellaneous

Use useful names
================

Please try to avoid using names such as col-left, link-red. What happens when you later decide to move that left column to right or use blue color for all current red links? Use something like menu-bar or external-link for example. 

Create the logical structure first
==================================

Do not jump right into coding, follow these steps:

1. Review every single page/element on the design to have a good overview/understanding of the design first.
2. Map out the general layout first then create an HTML structure that will support the layout
3. Start working on HTML first
4. Apply CSS rules 
   
.. code-block:: html
    
        <!DOCTYPE html> 
        <html>

            <head>
                <!-- Some head content here -->
            </head>

            <body>
                
                <header>
                    <h1>City Gallery</h1>
                </header>

                <nav>
                    London<br>
                    Paris<br>
                    Tokyo<br>
                </nav>

                <section>
                    <h1>London</h1>
                    <p>
                    London is the capital city of England. It is the most populous city in the United Kingdom,
                    with a metropolitan area of over 13 million inhabitants.
                    </p>
                    <p>
                    Standing on the River Thames, London has been a major settlement for two millennia,
                    its history going back to its founding by the Romans, who named it Londinium.
                    </p>
                </section>

                <footer>
                    Copyright © W3Schools.com
                </footer>
            </body>
        </html>   

Use shorthand CSS
=================

.. code-block:: css
    
    /* The code below is not recommended */
    margin-top: 10px;
    margin-right: 24px;
    margin-bottom: 12px;
    margin-left: 0;

    /* Use this instead */
    margin: 10px 24px 12px 0;

Avoid using inline CSS
======================

Inline CSS makes it very very difficult to maintain code.
    
    .. code-block:: html
    
        <div style="background:red;">
            Ipsum schmipsum
        </div>

Always use external stylesheets instead.

Understand the difference between block vs inline elements
==========================================================

Block elements are elements that naturally clear each line after they’re declared, spanning the whole width of the available space. Inline elements take only as much space as they need, and don’t force a new line after they’re used.

Here are the lists of elements that are either inline or block:

**span, a, strong, em, img, br, input, abbr, acronym**

And the block elements:

**div, h1…h6, p, ul, li, table, blockquote, pre, form**

Be careful with positioning
===========================

Understand `CSS positioning`_ 

Avoid absolute and fixed positioning as much as possible since it can be very hard to maitain and debug.


Write efficient CSS Rules
=============================

Please read up the following article `Efficient CSS`_ . In short:

1. ID > Class > Tag > System rules (with ID being the fastest selector).
2. Don't qualify ID with class, tag
3. Don't qualify class with tag
4. Use the most specific category possible
5. Avoid descendant selector
6. Tag rule should ``NEVER`` contain a child selector
7. Avoid child selector when possible
8. Rely on inheritance
9. Avoid vendor specific tags where ever not necessary


.. _Efficient CSS: https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Writing_efficient_CSS
.. _CSS positioning: http://www.cssreset.com/understanding-css-relative-and-absolute-positioning-explained/