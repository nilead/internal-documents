*********
Angularjs
*********

Directive
=========

Attribute
---------

It is **strongly** recommended to always prefix your attr with ``nl`` or ``data-nl``

Directive
---------

Directive parameters should be prefixed with ``nl``



Please refer to this standard guideline for Angularjs `standard guideline for Angularjs`_ 

.. _standard guideline for Angularjs: https://github.com/mgechev/angularjs-style-guide