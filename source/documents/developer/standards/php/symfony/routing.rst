Routing
=======

- All routing keys **SHOULD** be in **lowercase** in the form: ``{app}.{bundle}.{environment}.{resource}.{action}``
- Routes with parameters **SHOULD** define the default values if possible
- Routes with data formats (such as ``json``, ``xml``) **MUST** define list of serialization groups and version
- Routes **SHOULD** be defined strictly with parameter requirements if possible
- Routes **MUST** define list of allowed methods
- If any part has more than 1 word, the words **MUST** be seperated by the **underscore** ``_``
- Route's prefix **MUST** be defined inside bundle config, not in app's config

**Example:** ``nilead.sales.backend.order.cancel_invoice``

Standard Actions
----------------

We have 5 standard actions and should be named as followed:

- **index:**  Used for resource listing, paginating filtering, sorting
- **show:**   Used for displaying a single resource
- **create:** Create new resource
- **update:** Update an exists resource
- **delete:** Delete an exists resource

Symfony Routing
---------------

An example routing for index action in Symfony

.. code-block:: yaml

    # {app}.{bundle}.{environment}.{resource}.{action}
    # Ommit first backslash if the pattern does not start with an argument
    nilead.product.backend.product.index:
        pattern: /{page}.{_format}  
        methods: [GET]              
        defaults:
            page: 1
            _controller: nilead.product.backend.controller.product:indexAction
            _format: html
            _nilead:
                serialization_groups: ['templating']
                serialization_version: 1.0
        requirements:
            page: \d+
            _format: (html|partial|json)
        options:
            expose: true


Angularjs Routing
-----------------

An example routing for product create action in Angular

.. code-block:: javascript

    $stateProvider
        .state('order_list', {
            url: "/order/list/:page",
            params: {
                page: {value: 1}
            },
            templateUrl: 'someroute',
            controller: "OrderListCtrl",
            displayName: 'Listing'
        })
        .state('product_create', {
            url: '/product/create',
            controller: 'ProductCreateCtrl',
            templateProvider: function (Resource) {
                var url = Routing.generate('nilead.product.backend.product.create', {_format: 'partial'});

                return Resource.get(url).then(function (response) {
                    return response.data.template;
                });
            },
            next: function (response) {
                return {
                    name: 'product_update',
                    params: {
                        id: response.product.id
                    }
                }
            }
        })
    ;

Since we use Angular UI Router, the state name should use the dot '.' ONLY for nested state cases (nested-states_)


.. _nested-states: https://github.com/angular-ui/ui-router/wiki/Nested-States-%26-Nested-Views