*******
Bundle
*******

1. Always start with Nilead and end with Bundle (i.e. NileadMyAcmeBundle)

2. Always place interfaces in the corresponding Component package (NileadMyAcmeComponent) you intend to expose the API to other developers (internal or external)

4. Always create `composer.json <https://getcomposer.org/>`_

3. Always use `psr-4 <http://www.php-fig.org/psr/psr-4/>`_