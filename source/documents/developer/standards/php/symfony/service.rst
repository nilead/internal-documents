Service
=======
- All service parameter keys **MUST** be in **lowercase** in the form: ``{app}.{bundle}.[ serviceName ].{resource}.class``
- All service keys **MUST** be in **lowercase** in the form: ``{app}.{bundle}.[ serviceName ].{resource}``
- If any part has more than 1 word, the words **MUST** be seperated by the **underscore** ``_``
- **serviceName** ** SHOULD ** be meaningful. We recommend a few case values below: 

``handler`` ``mailer`` ``security`` ``helper`` ``factory`` ``strategy`` 
``subscriber`` ``event_subcriber`` ``event`` ``event_listener``
``model`` ``form.type`` ``form.listener`` ``controller`` ``controller.listener``

**Example:** 
    ``nilead.sales.form.type.cancel_invoice.class``
    ``nilead.sales.form.type.cancel_invoice``
    ``nilead.sales.controller.listener.cancel_invoice.class``
    ``nilead.sales.controller.listener.cancel_invoice``
    

Define Custom Controller Service
===================================


Define Custom Model Service
==============================


Define Custom Form Service
==============================