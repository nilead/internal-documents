Validation 
==========

- All validation message translate keys **SHOULD** be in **lowercase** in the form: ``{app}.{resource}.validation.{field}.{constraint}``
- If any part has more than 1 word, the words **MUST** be seperated by the **underscore** ``_``

**Example:** 
``nilead.order.validation.name.not_null``
