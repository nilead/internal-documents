Database fields stabdards
=========================

Use int for all timestamp, datetime fields. The reason is that we don't want to store the time with the server timezone (which can be come quite a problem once we start using multiple servers to host our db)

- Field ``Description`` **SHOULD** be defined as ``Text`` type.
- Field ``Summary`` **SHOULD** be defined as ``String`` type.
- Field ``Name`` **SHOULD** be defined as ``String`` type with lenght ``255``.
- Field ``Datetime`` **SHOULD** be defined as ``int``. However, in case the specific field must store date before the year **1970** please use ``Datetime``.