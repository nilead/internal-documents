

Data type
=========

Please refer to the following sources [#]_ (Chapter 3)

.. [#] High Performance MySQL Optimization, Backups, Replication, and More, 2nd Edition


Use simple data types
---------------------

When possible, use simple data types. 

Comparing the commonly used types:


+---------------------+--------------------------------------------+--------------------------+
| Purpose             | Type                                       | Type                     |
+=====================+============================================+==========================+
| Storing short       | CHAR                                       | VARCHAR                  |
| strings             |                                            |                          |
+---------------------+--------------------------------------------+--------------------------+
|                     | Better performance when strings length are | Variable lengths         |
|                     | identical or almost identical              |                          |
+---------------------+--------------------------------------------+--------------------------+
| Store large amount  | BLOB                                       | TEXT                     |
| of data             |                                            |                          |
+---------------------+--------------------------------------------+--------------------------+
|                     | Store data without any collation or        |                          |
|                     | charset                                    |                          |
+---------------------+--------------------------------------------+--------------------------+

Consider using TIMESTAMP or BIGINT to store date/time instead of DATETIME for 2 reasons:

1. Less space is used
2. Avoid issue with TimeZone conversion

Avoid using string types for Identifiers (Primary Index) since they are slow.

Avoid Null if possible
----------------------

It's harder for MySQL to index NULL columns. Consider using default values such as empty strings or 0

Indexing
========

Types
-----

BTree Index
^^^^^^^^^^^

The most common is BTree index. If not mentioned or specificed an index will mostly use BTree.

1. They are not useful if the lookup does not start from the left most side of the indexed columns (column order in an index is EXTREMELY important)
2. Can't skip columns in the index
3. The storage engine can't optimize access with any columns to the right of the first range condition (for example, the LIKE condition)

Strategies
----------

Isolate the columns
^^^^^^^^^^^^^^^^^^^

"Isolating" the column means it should not be part of an expression or be inside a function in the query.

**BAD**

.. code-block:: mysql

    SELECT actor_id FROM sakila.actor WHERE actor_id + 1 = 5;

**GOOD**

.. code-block:: mysql

    SELECT actor_id FROM sakila.actor WHERE actor_id = 4;

Prefix Indexes and Index Selectivity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please refer to the book. This is mostly useful when indexing long strings. (which we don't need in our case)

.. important:: Please always define surrogate key, which is a primary key whose value is not derived from your application's data. The easiest way to do this is usually with an AUTO_INCREMENT column. This will ensure that rows are inserted in sequential order and will offer better performance for joins using primary keys.

**Be careful when using IN**

.. code-block:: mysql

    WHERE eye_color   IN('brown','blue','hazel')
    AND hair_color IN('black','red','blonde','brown')
    AND sex        IN('M','F')

The above query can result in 24 combinations and the WHERE clause will have to check each of them.

**Avoiding Multiple Range Conditions**

.. code-block:: mysql

    WHERE  eye_color   IN('brown','blue','hazel')
    AND hair_color  IN('black','red','blonde','brown')
    AND sex         IN('M','F')
    AND last_online > DATE_SUB('2008-01-17', INTERVAL 7 DAY)
    AND age         BETWEEN 18 AND 25

There's a problem with this query: it has two range conditions. MySQL can use either the last_online criterion or the age criterion, but not both.

**Sorting and Paginating can be very slow**

Please refer to this `ORDER BY Optimization`_ for possible optimization.

.. _ORDER BY Optimization: http://dev.mysql.com/doc/refman/5.0/en/order-by-optimization.html