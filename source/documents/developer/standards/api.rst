****
API
****

+------------+------------+--------+---------------------------------------+
| Method     | URI        | Action | Route Name                            |
+============+============+========+=======================================+
| GET        |            | index  | nilead.{bundle}.api.{resource}.index  |
+------------+------------+--------+---------------------------------------+
| GET        | {id}       | show   | nilead.{bundle}.api.{resource}.show   |
+------------+------------+--------+---------------------------------------+
| POST       | create     | create | nilead.{bundle}.api.{resource}.create |
+------------+------------+--------+---------------------------------------+
| GETPOSTPUT | {id}update | update | nilead.{bundle}.api.{resource}.update |
+------------+------------+--------+---------------------------------------+
| POSTDELETE | {id}       | delete | nilead.{bundle}.api.{resource}.delete |
+------------+------------+--------+---------------------------------------+


The ``api`` routing should be follow this pattern
``nilead.api.{bundle}.{resource}.{action}``

And the ``URI`` will be something like this
``api{version}{resource}{action}.json``

--------------

Index Action
------------

Receive a list of resources with ``{page}`` and ``{limit}``.

``backend{resource}index{page}{limit}.{_format}``

.. code-block:: yaml

    method [GET]
    pattern index
    action indexAction
    route nilead.{env}.{bundle}.{resource}.index
    arguments
        page {integer}   # default `1`, current page number
        limit {integer}  # default `10`, the limit resources for each page
        _format          # default `html`
            - html        # full web based view
            - partial     # a partial view
            - json        # @api list of resources object in json format
    expose true

--------------

Show Action
-----------

Receive a single resource.

``backend{resource}{id}.{_format}``

.. code-block:: yaml

    method [GET]
    pattern {id}
    action showAction
    route nilead.{env}.{bundle}.{resource}.show
    arguments
        id {integer}   # required, the resource ID
        _format        # default `html`
            - html      # full web based view
            - partial   # a partial view
            - json      # @api resource object in json format
    expose true

--------------

Create Action
-------------

Create a new resource.

``backend{resource}create.{_format}``

.. code-block:: yaml

    pattern create
    action createAction
    route nilead.{env}.{bundle}.{resource}.create
    expose true

None API URI and method

``backend{resource}create.html``

``backend{resource}create.partial``

.. code-block:: yaml

    method [GETPOST]
    arguments           # All of arguments can be omitted
        _format         # default `html`
            - html       # full web based view
            - partial    # a partial view

API URI and method

``backend{resource}create.json``

.. code-block:: yaml

    method [POST]
    arguments           # All of arguments can be omitted
        _format         # default `html`
            - json       # @api post a resource object to create via api

--------------

Update Action
-------------

Update an exists resource.

``backend{resource}{id}update.{_format}``

.. code-block:: yaml

    pattern update
    action updateAction
    route nilead.{env}.{bundle}.{resource}.update
    expose true

None API URI and method

``backend{resource}{id}update.html``

``backend{resource}{id}update.partial``

.. code-block:: yaml

    method [GETPOST]
    arguments
        id {integer}    # required, the resource ID
        _format         # default `html`
            - html       # full web based view
            - partial    # a partial view

API URI and method

``backend{resource}{id}update.json``

.. code-block:: yaml

    method [PUT]
    arguments
        id {integer}    # required, the resource ID
        _format         # default `html`
            - json       # @api put an updated resource object via api

--------------

Delete Action
-------------

Delete an exists resource.

``backend{resource}{id}.{_format}``

.. code-block:: yaml

    pattern delete
    action deleteAction
    route nilead.{env}.{bundle}.{resource}.delete
    expose true

None API URI and method

``backend{resource}{id}delete.html``

``backend{resource}{id}delete.partial``

.. code-block:: yaml

    method [POST]
    arguments
        id {integer}    # required, the resource ID
        _format         # default `html`
            - html       # full web based view
            - partial    # a partial view

API URI and method

``backend{resource}{id}.json``

.. code-block:: yaml

    method [DELETE]
    arguments
        id {integer}    # required, the resource ID
        _format         # default `html`
            - json       # @api delete an exists resource via api
