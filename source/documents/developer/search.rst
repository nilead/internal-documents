*******
Search
*******

Nilead search feature is provided by the SearchBundle which supports several search engines. As of now, there are 2 search engines supported:

1. `Doctrine`_ 
2. `ElasticSearch`_ 

Configuration
=============

Before using nilead_search, make sure you have FosElasticaBundle installed and configured first.

.. code-block:: yaml

    source

sadasdasdasdsa

Understanding the flow
======================


Custom parser
=============



.. _Doctrine: http://doctrine-project.org
.. _ElasticSearch: http://elasticsearch.org