=========
Inventory
=========

Inventory management is complex, Nilead does not aim to be a full blown inventory management system; however, we do have a system that can help the majority of small businesses to manage inventory.


***************************
Simple Inventory Management
***************************

Most stores need to keep of the current quantity on hand of each item only, and for that purpose a very simple inventory system is needed.

Nilead, by default, support store owners to set on hand quantity per product variant


*****************************
Advanced Inventory Management
*****************************

For more advanced tracking purpose, Nilead also allows store owner(s) to keep track of the following information:

- location (facility, container)
- in/out date of each current available on hand lot
- inventory adjustment history
- inventory variance history (difference between physical inventory vs on-system inventory)

Inventory Strategy
==================

Inventory strategy can be very useful when you sell products that should be sold using a custom in/out strategy (FIFO, LIFO, ...). The inventory strategy will be used to generate a picklist for the shipping order as well as marking the specific inventory item as sold. 

By default, Nilead supports FIFO and LIFO strategy; however, it is possible to support customized strategy that fits your business needs.

Inventory Restock
==================

Inventory restock happens when you:

- cancel a shipment (this action cannot be undone)
- cancel a particular item in a shipment
- process returned merchandies

In the first 2 cases, the inventory item will be restocked on the assumption that the items will be stocked back to their previous location.

On the last case, the person in charge may have to decide to facility to stock back the item.