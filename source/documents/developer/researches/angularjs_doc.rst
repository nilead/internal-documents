*****************************
AngularJS documentation tools
*****************************

`Writing AngularJS Documentation`_ 

`More details on the annotations`_ 

`Sample code`_ 


.. _Writing AngularJS Documentation: https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation
.. _More details on the annotations: https://github.com/angular/dgeni-packages/blob/master/NOTES.md
.. _Sample code: http://www.podpea.co.uk/blog/starting-off-with-ngdocs/