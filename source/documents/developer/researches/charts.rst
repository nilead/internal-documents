Charts
======

We are currently using chartis.js for our charts in the backend. We have reviewed several libraries and decided to use chartis for now. Below is our comparison table which list the noteworthy libraries we looked at:

+--------------+-------------+----------------------------------------------------+
| Libary       | License     | Reviews                                            |  
+==============+=============+====================================================+
| Highchart    | Commercial  | - Supports many types of charts                    | 
|              |             | - Beautiful layout                                 | 
|              |             | - Very complex options                             |
+--------------+-------------+----------------------------------------------------+
| Chartis      | Free        | - Support a limited number of types                |
|              |             | - Beautiful layout                                 | 
|              |             | - Simple options                                   |
+--------------+-------------+----------------------------------------------------+
| Dimple       | Free        | - Support a big number of types, require d3        |
|              |             | - Nice layout                                      | 
|              |             | - Fairly complex options                           |
+--------------+-------------+----------------------------------------------------+