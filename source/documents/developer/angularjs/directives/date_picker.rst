*********************
Date Picker directive
*********************

Located in ``nilead.common``

Purpose
=======

Create a date picker input

Usage
======

Apps that use this directive should include ``nilead.common``

.. code-block:: html

    Restrict: A
    Requirements: [nlModel, nlProperty, nlOptions, nlType]


Syntax
~~~~~~

.. code-block:: html

    nl-model
    nl-property
    nl-options
    nl-type

The model and property together should allow the directive to retrieve the time string to use as input. For example, if your model is ``blog`` and the time is stored in ``blog.published_date`` then you use:

.. code-block:: html
	
	data-nl-model="blog" data-nl-property="published_date" 
 

This module is using `DateTime Picker module`_ and accepts the same list of options which can be passed into nl-options.

By default, this module accepts unix timestamp, if you want to use the default DateTime format you can pass that into the nl-type


**Example**

.. code-block:: html

    <input type="text" data-nl-initial="Some Thing" data-ng-model="product.name">


If the directive define without any value and the element has a value, that value will bind into your model

.. code-block:: html

    <div class="input-group date" data-nl-datepicker data-nl-model="blog" data-nl-property="published" data-nl-options="{defaultDate: '11/1/2013'}">
        <input class="form-control" size="16" type="text">
        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
    </div>		


.. _DateTime Picker module: https://github.com/Eonasdan/bootstrap-datetimepicker/