*******************
Paginator directive
*******************


Located in ``nilead/common``


Purpose
=======

Display a pagination navigation link list


Usage
======

Apps that use this directive should include ``nilead.common``


.. code-block:: html

    Restrict: none
    Requirements: [nlPaginator, nlCallback, route]


This directive also expects a template with id ``paginator.html`` to be included.


Syntax
~~~~~~

.. code-block:: html

    nl-paginator="paginator.params"
    nl-callback="callbackMethod"
    route="some_state_route"

The value passed into nl-paginator is expected to be an array with the following keys and corresponding sample values:

.. code-block:: yaml

	current: 3
	currentItemCount: 1
	endPage: 3
	first: 1
	firstItemNumber: 11
	firstPageInRange: 1
	last: 3
	lastItemNumber: 11
	lastPageInRange: 3
	numItemsPerPage: 5
	pageCount: 3
	pageRange: 3
	pagesInRange: [1, 2, 3]
	previous: 2
	startPage: 1
	totalCount: 11


**Example**

.. code-block:: html

    <!-- callback is a function that will be triggered onclick. Callback will receive the current page and is expected to return true/false. if the callback method exists and return false then the event will be stopped -->
    <div data-nl-paginator="params" data-nl-callback="callBack" route="some_route_state"></div>