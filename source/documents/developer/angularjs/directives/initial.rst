*****************
Initial directive
*****************

Located in ``nilead.common``


Purpose
=======

Automatic initialize value to angular model through ``ngModel``


Usage
======

Apps that use this directive should include ``nilead.common``

.. code-block:: html

    Restrict: A
    Requirements: [ngModel]


Syntax
~~~~~~

.. code-block:: html

    nl-initial
    nl-initial="Some Thing"


If the directive defined with some value, that value will bind into your model


**Example**

.. code-block:: html

    <!-- After this element rendered, your model ``product.name`` should be "Some Thing" -->
    <input type="text" data-nl-initial="Some Thing" data-ng-model="product.name">


If the directive define without any value and the element has a value, that value will bind into your model

.. code-block:: html

    <!-- After this element rendered, your model ``product.name`` should be "Some Value" -->
    <input type="text" data-nl-initial data-ng-model="product.name" value="Some Value">

