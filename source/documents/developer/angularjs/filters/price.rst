.. _price_filter:
*************
Price filter
*************


Located in ``nilead/common``


Purpose
=======

Price filter takes a *base unit* price amount and display it correctly with currency symbol.

Given an ``integer`` and ``currencyCode`, output a formated and calculated currency amount


Usage
======

Apps that use this directive should include ``nilead.common``


Syntax
~~~~~~

.. code-block:: jinja
	integerAmount | price: 'currencyCode' # currencyCode is optional


+--------------+-------------+-----------------------------+-----------------------+
| Parameter    | Type        | Details                     | Default               |  
+==============+=============+=============================+=======================+
| amount       | integer     | (optional) base unit amount |                       |
+--------------+-------------+-----------------------------+-----------------------+
| currencyCode | string      | (optional) currency code    | default currency      |
+--------------+-------------+-----------------------------+-----------------------+


**Example**

.. code-block:: jinja

	10000 | price: 'USD' # will print $ 100
	10000 | price: 'VND' # will print đ 10000
	10000 | price        # will print đ 10000 if the default is VND

*Please make sure to do console nilead:assetic:dump-local-libs to dump the necessary code for this filter to work*