*************
Image filter
*************
This is paragraph text *before* the table.

+----------+------------------------------------------------+-------------------------------------------------+
| Column 1 | Coulmn 2                                       |                                                 |
+==========+================================================+=================================================+
| Foo      | Put two (or more) spaces as a field separator. |                                                 |
+----------+------------------------------------------------+-------------------------------------------------+
| Bar      | Even very very long lines like these are fine, | as long as you do not put in line endings here. |
+----------+------------------------------------------------+-------------------------------------------------+

This is paragraph text *after* the table.

:nldocpath:angularjs/directives/chart:/Chart document/

:nlimage(class=abc,style=xyz:123;xyza:1234):icon-info.png:hello

Located in ``nilead/common``

Purpose
=======

Image filter takes an image path and attempts to fill into the corresponding image size.

Usage
======

Apps that use this directive should include ``nilead.common``


Syntax
~~~~~~

.. code-block:: jinja

	imagePath | image: 'size' 

.. important:: please review :doc:`/book/image` for available sizes


**Example**

.. code-block:: jinja

    'path_to/some_image_replaceable_filter.jpg' | image: 'grande' # 'path_to/some_image_grande.jpg'