=======
Sales
=======

Sales is a vital part of Nilead's eco system. Basically, all the "commerce" activities happen here.

******************
Order & Order Item
******************

An order in our system can be of any type. Currently there are 2 basic types available:

1. Purchase
2. Sales
   
An order is comprised of a list of order item. Each order item will contain information such as the UnitPrice (in default currency unit), the quantity, the link to the actual product in the system and other useful information. It's important to note that deleting a product in the database will not remove the corresponding order items.

******************
Order Adjustment
******************

It's important to note that we take the concept of `Order Adjustment from Spree <http://guides.spreecommerce.com/developer/adjustments.html>`_ and tweak it to our needs.

