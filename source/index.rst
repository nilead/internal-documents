Nilead Documentation
====================

`Nilead <http://rubikin.com>`_ is the e-commerce solution for PHP, based on `framework Symfony2 <http://symfony.com>`_.

The Book
--------

Developers guide to leveraging the flexibility of Nilead.

.. toctree::
   :titlesonly:

   book/symfony
   book/introduction
   book/resource
   book/asset
   book/image
   book/pricing
   book/product   
   book/inventory
   book/license

Standards
---------

Coding standards for Developers

.. toctree::
   :glob:

   book/standards/*

Angularjs
---------

Filters and directives for Angularjs

.. toctree::
   :glob:
   
   book/angularjs/**
   book/bundles/**